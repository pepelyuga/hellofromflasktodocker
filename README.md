# HelloFromFlaskToDocker

Test project aimed at obtaining basic skills using docker and flask

### Installing

1. Install Docker CE

<https://docs.docker.com/install/>

2. Input in command line
```
docker run -p 80:80 -d -n Flask_Dockerovich pepelyuga/hello-flask
```
